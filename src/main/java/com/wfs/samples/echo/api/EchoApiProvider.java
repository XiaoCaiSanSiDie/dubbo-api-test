package com.wfs.samples.echo.api;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.ServiceConfig;
import com.wfs.samples.echo.xml.api.EchoService;
import com.wfs.samples.echo.xml.impl.EchoServiceImpl;

import java.io.IOException;

public class EchoApiProvider {
    public static void main(String[] args) throws IOException {
        ServiceConfig<EchoService> serviceConfig=new ServiceConfig<EchoService>();
        serviceConfig.setApplication(new ApplicationConfig("java-echo-provider"));
        serviceConfig.setRegistry(new RegistryConfig("zookeeper://127.0.0.1:2181"));
        //指定服务暴露的接口
        serviceConfig.setInterface(EchoService.class);
        //指定真实服务对象
        serviceConfig.setRef(new EchoServiceImpl());
        //暴露服务
        serviceConfig.export();
        System.out.println("java-echo-provider is running");
        System.in.read();
    }
}
