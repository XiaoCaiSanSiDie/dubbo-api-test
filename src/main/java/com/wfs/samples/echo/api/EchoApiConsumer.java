package com.wfs.samples.echo.api;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.ServiceConfig;
import com.wfs.samples.echo.xml.api.EchoService;
import com.wfs.samples.echo.xml.impl.EchoServiceImpl;

import java.io.IOException;
import java.lang.ref.Reference;

public class EchoApiConsumer {
    public static void main(String[] args) throws IOException {
        ReferenceConfig<EchoService> referenceConfig=new ReferenceConfig<EchoService>();

        referenceConfig.setApplication(new ApplicationConfig("java-echo-comsumer"));
        referenceConfig.setRegistry(new RegistryConfig("zookeeper://127.0.0.1:2181"));
        //指定服务暴露的接口
        referenceConfig.setInterface(EchoService.class);
        EchoService echoService = referenceConfig.get();
        String say_hello = echoService.echo("say hello");
        System.out.println(say_hello);

    }
}
