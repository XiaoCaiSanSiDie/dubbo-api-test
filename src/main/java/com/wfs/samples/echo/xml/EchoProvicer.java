package com.wfs.samples.echo.xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class EchoProvicer {
    public static void main(String[] args) throws IOException {
        //指定暴露配置文件
        ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("echo-provider.xml");
        //启动Spring容器并暴露服务
        context.start();
         System.in.read();
    }
}
