package com.wfs.samples.echo.xml.impl;

import com.alibaba.dubbo.rpc.RpcContext;
import com.wfs.samples.echo.xml.api.EchoService;


import java.text.SimpleDateFormat;
import java.util.Date;

public class EchoServiceImpl implements EchoService {

    public String echo(String msg) {
        String now=new SimpleDateFormat("HH:mm:ss").format(new Date());
        System.err.println(now+" hello "+msg+" ,"+ RpcContext.getContext().getRemoteAddress());
        return msg;
    }
}
