

import com.wfs.samples.echo.xml.api.EchoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EchoConsumer {
    public static void main(String[] args) {
        //加载配置
        ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("echo-consumer.xml");
        //获取消费代理
        context.start();
        EchoService echoService= (EchoService) context.getBean("echoService");
        echoService.echo("hello word");
        System.out.println("---------");
    }
}
