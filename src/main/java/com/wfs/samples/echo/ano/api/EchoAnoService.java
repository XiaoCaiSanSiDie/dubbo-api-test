package com.wfs.samples.echo.ano.api;

public interface EchoAnoService {
    String echo(String msg);
}
