package com.wfs.samples.echo.ano.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.dubbo.rpc.RpcContext;
import com.wfs.samples.echo.ano.api.EchoAnoService;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class EchoAnoServiceImpl implements EchoAnoService {
    public String echo(String msg) {
        String now=new SimpleDateFormat("HH:mm:ss").format(new Date());
        System.err.println(now+" hello "+msg+" ,"+ RpcContext.getContext().getRemoteAddress());
        return msg;
    }
}
