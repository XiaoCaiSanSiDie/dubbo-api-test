package com.wfs.samples.echo.ano;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ConsumerConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.wfs.samples.echo.ano.consumer.EchoAnoConsumer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

public class AnnotationConsumer {
    public static void main(String[] args) {
        //基于注解配置初始化Spring上下文
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        //服务调用
        EchoAnoConsumer bean = context.getBean(EchoAnoConsumer.class);
        String say_hello = bean.echo("say hello");
        System.err.println(say_hello);
    }

    @Configuration
    @EnableDubbo(scanBasePackages = "com.wfs.samples.echo.ano.consumer")
    @ComponentScan(value = "com.wfs.samples.echo.ano.consumer")
    static  class ConsumerConfiguration{
        @Bean
        public ConsumerConfig consumerConfig(){
            return new ConsumerConfig();
        }
        @Bean
        public ApplicationConfig applicationConfig(){
            ApplicationConfig applicationConfig=new ApplicationConfig();
            applicationConfig.setName("echo-annotation-consumer");
            return  applicationConfig;
        }
        @Bean
        public RegistryConfig registryConfig(){
            RegistryConfig registryConfig=new RegistryConfig();
            //使用zookeeper作为注册中心同时给出IP 和端口
            registryConfig.setProtocol("zookeeper");
            registryConfig.setAddress("localhost");
            registryConfig.setPort(2181);
            return  registryConfig;
        }

    }
}
