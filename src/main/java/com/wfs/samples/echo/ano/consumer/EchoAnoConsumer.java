package com.wfs.samples.echo.ano.consumer;

import com.alibaba.dubbo.config.annotation.Reference;
import com.wfs.samples.echo.ano.api.EchoAnoService;
import org.springframework.stereotype.Component;

@Component
public class EchoAnoConsumer {
    @Reference
    EchoAnoService echoAnoService;
    public String echo(String name ){
        String echo = echoAnoService.echo(name);
        return echo;
    }
}
